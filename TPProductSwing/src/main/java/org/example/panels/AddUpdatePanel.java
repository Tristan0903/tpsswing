package org.example.panels;

import com.example.productapp.dto.request.RequestProductDTO;
import com.example.productapp.dto.response.ResponseProductDTO;
import lombok.Data;
import org.example.service.ProductService;

import javax.swing.*;

@Data
public class AddUpdatePanel {
    private ProductService _productService;
    private JPanel AddUpdatePanel;
    private JLabel ProductID;
    private JTextField productIdField;
    private JTextField nameField;
    private JLabel desciption;
    private JLabel name;
    private JTextArea descriptionField;
    private JTextField quantityField;
    private JTextField unitPriceField;
    private JButton addButton;
    private JButton updateButton;
    private JButton searchButton;
    private JButton monPanierButton;
    private JButton nextButton;
    private JButton lastButton;
    private JLabel message;

    public AddUpdatePanel(){
        _productService = new ProductService();
        searchButton.addActionListener((e)->{
            try{
                ResponseProductDTO dto = _productService.findById(Integer.parseInt(productIdField.getText()));
                nameField.setText(dto.getName());
                quantityField.setText(String.valueOf(dto.getStock()));
                unitPriceField.setText(String.valueOf(dto.getPrice()));
            }catch (Exception ex){
                message.setText(ex.getMessage());
                resetMessage();
            }

        });

        updateButton.addActionListener((e)->{
            try{
                ResponseProductDTO productDTO = _productService.update(Integer.parseInt(productIdField.getText()),Integer.parseInt(quantityField.getText()));
                message.setText("Le produit a bien été mis à jour");
                resetMessage();
            }catch (Exception ex){
                message.setText(ex.getMessage());
                resetMessage();
            }


        });
        addButton.addActionListener((e)->{
            try{
                RequestProductDTO productDTO = new RequestProductDTO();
                productDTO.setName(nameField.getText());
                productDTO.setPrice(Double.valueOf(unitPriceField.getText()));
                productDTO.setStock(Integer.parseInt(quantityField.getText()));
                ResponseProductDTO responseProductDTO = _productService.add(productDTO);
                resetMessage();
            }catch (Exception ex){
                message.setText(ex.getMessage());
                resetMessage();
            }

        });
    }

    private void resetMessage(){
        Timer timer = new Timer(3000,(ev)->message.setText(""));
        timer.start();
    }


}
