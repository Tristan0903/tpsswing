package org.example.panels;

import lombok.Data;

import javax.swing.*;
@Data
public class CartPanel {
    private JPanel panierPanel;
    private JComboBox nameField;
    private JComboBox quantityField;
    private JButton addProductButton;
    private JList name;
    private JList quantity;
    private JList cost;
    private JLabel total;
}
