package org.example;

import org.example.panels.AddUpdatePanel;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args)
    {

        JFrame patientFrame = new JFrame("Product");
        patientFrame.setSize(new Dimension(600,600));
        patientFrame.setContentPane(new AddUpdatePanel().getAddUpdatePanel());
        patientFrame.setVisible(true);
    }
}