package org.example.repositories;

import org.example.entities.DossierMedical;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DossierMedicalRepository extends CrudRepository<DossierMedical, Integer> {



}
