package org.example.window;

import lombok.Data;
import org.example.entities.DossierMedical;
import org.example.services.DossierMedicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

@Data
@Component
public class DossierMedicalFrom {

    @Autowired
    private DossierMedicalService dossierMedicalService;
    private JTextField codeAccesPatient;

    private JLabel jLabel;

    private FlowLayout gridLayout;

    private JFrame dossierMedicalFrame;

    private JPanel dossierMedicalPanel;

    private JButton validate;

    private GridBagConstraints gridBagConstraints;

    public DossierMedicalFrom(){
        gridLayout = new FlowLayout();
        dossierMedicalPanel = new JPanel();
        dossierMedicalPanel.setLayout(gridLayout);
        jLabel = new JLabel("Code accès patient: ");
        codeAccesPatient = new JTextField(5);
        validate = new JButton("Créer");
        validate.addActionListener(e -> {
            createDossier();
        });
        dossierMedicalPanel.add(jLabel);
        dossierMedicalPanel.add(codeAccesPatient);
        dossierMedicalPanel.add(validate);
    }

    public void createDossier(){
        DossierMedical dossierMedical = new DossierMedical();
        dossierMedical.setCodeAccessPatient(codeAccesPatient.getText());
        dossierMedicalService.create(dossierMedical);
    }

}
