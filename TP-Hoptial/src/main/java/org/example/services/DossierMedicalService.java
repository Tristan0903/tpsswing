package org.example.services;

import org.example.entities.DossierMedical;
import org.example.entities.Patient;
import org.example.repositories.DossierMedicalRepository;
import org.example.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DossierMedicalService {

    @Autowired
    DossierMedicalRepository _dossierMedicalRepository;

    @Autowired
    PatientRepository _patientRepository;

    public DossierMedical create(DossierMedical dossierMedical){
        _dossierMedicalRepository.save(dossierMedical);
        return dossierMedical;
    }

}
