package org.example;

import org.example.window.DossierMedicalFrom;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JFrame dossierMedical = new JFrame("Dossier Medical Creation");
        dossierMedical.setContentPane(new DossierMedicalFrom().getDossierMedicalPanel());
        dossierMedical.setSize(500, 1000);
        dossierMedical.setVisible(true);
    }
}