package org.example.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fiche_consultation")
public class FicheConsultation extends FicheSoin {

    @Column(name ="compte_rendu")
    private String compteRendu;

    @OneToMany
    private List<Prescription> prescriptions;

    //@OneToOne
    //private Consultation consultation;
}
