package org.example.entities;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "patient")
public class Patient {
    @Id
    private String codeAccesPatient;
    private String nss;
    private String nom;
    private String prenom;
    @Column(name = "date_naissance")
    private Date dateNaissance;
    private char sexe;
    private String adresse;
    @Column(name = "numero_telephone")
    private String numeroTelephone;

    @OneToOne
    @JoinColumn(name = "dossier_medical_id")
    DossierMedical dossierMedical;

    public void setNom(String nom) throws Exception {
        if(nom != null && nom.length() > 2)
            this.nom = nom;
        else
            throw new Exception();
    }
}
