package org.example.window;

import lombok.Data;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class Calculatrice {

    JPanel calcPanel;

    private JLabel blackScreen;
    private GridBagLayout gridBagLayout;
    private GridBagConstraints gridBagConstraints;
    String caracs[] = {"C", "+/-", "%" ,"/", "7", "8", "9", "X", "4", "5", "6", "-", "1", "2", "3", "+", "0", ".", "="};
    List<String> caracteres = Arrays.stream(caracs).toList();

    public Calculatrice(){
        gridBagLayout = new GridBagLayout();
        gridBagConstraints = new GridBagConstraints();
        calcPanel.setLayout(gridBagLayout);
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        createBlackScreen();
        createButtons();
    }

    private void createBlackScreen(){
        blackScreen = new JLabel("0");
        blackScreen.setBackground(Color.BLACK);
        blackScreen.setOpaque(true);
        blackScreen.setFont(new Font(null, 0, 30));
        blackScreen.setHorizontalAlignment(SwingConstants.RIGHT);
        blackScreen.setVerticalAlignment(SwingConstants.BOTTOM);
        gridBagConstraints.weightx = 5;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        calcPanel.add(blackScreen,gridBagConstraints);
    }

    private void actionButton(ActionEvent event){
        JButton b = (JButton) event.getSource();
        blackScreen.setText(b.getText());
    }

    private void createButtons(){
        int x = 0;
        int y = 1;
        for (String c:caracteres
        ) {
            JButton button = new JButton(c);
            button.addActionListener((e) -> {

            });
            gridBagConstraints.gridx = x;
            gridBagConstraints.gridy = y;
            calcPanel.add(button, gridBagConstraints);
            gridBagConstraints.gridy = y;
            gridBagConstraints.gridx = x;
            gridBagConstraints.weightx = 0.5;
            gridBagConstraints.weighty = 0.5;
            if(c.equals("0")){
                gridBagConstraints.gridwidth = 2;
                x++;
            }else{
                gridBagConstraints.gridwidth = 1;
            }
            if((x+1)%4 == 0){
                button.setBackground(Color.ORANGE);
                button.setForeground(Color.WHITE);
                y++;
                x=0;
            }else{
                x++;
            }
        }
    }

}
