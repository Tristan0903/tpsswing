package org.example;

import org.example.window.Calculatrice;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JFrame calculatrice = new JFrame("Calculatrice");
        calculatrice.setSize(300, 650);
        calculatrice.setContentPane(new Calculatrice().getCalcPanel());
        calculatrice.setVisible(true);
    }
}