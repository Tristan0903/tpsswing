package org.example.window;

import lombok.Data;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Data
public class FirstWindow {
    private JPanel firstPanel;

    private int count;

    public FirstWindow(){
//        button1.setText("Ouai mon gars");
//        button1.addActionListener((e -> {
//            count++;
//            button1.setText(button1.getText() + " clicks: " +count);
//        }));
//       );
//            firstPanel.add(b);
//        }
        //Utilistation des GridBagLayout
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[3];
        gridBagLayout.rowHeights = new int[6];
        firstPanel.setLayout(new GridBagLayout());
        for (int i = 1; i <=3 ; i++) {
            for (int j = 1; j <=3 ; j++) {
                JButton b = new JButton("b" +i);
                GridBagConstraints c = new GridBagConstraints();
                //c.fill = GridBagConstraints.HORIZONTAL;
                c.gridy = j;
                c.gridx = i;
                c.weightx = 1;
                c.weighty = 1;
                c.fill = GridBagConstraints.BOTH;
                if(i == 0 && j == 0){
                    c.gridwidth = 2;
//                    c.fill = GridBagConstraints.HORIZONTAL;
                }
                if(i == 1 && j==1){
                    c.gridheight = 2;
//                    c.fill = GridBagConstraints.VERTICAL;
                }
                b.setBackground(Color.BLACK);
                b.addActionListener((e) -> {
                    System.out.println(((JButton)e.getSource()).getText());
                });
                firstPanel.add(b, c);
            }

        }
    }

}
