package org.example;

import org.example.window.FirstWindow;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JFrame mainFrame = new JFrame("La première frame");
        mainFrame.setSize(1000, 1000);
        mainFrame.setContentPane(new FirstWindow().getFirstPanel());
        mainFrame.setVisible(true);
    }
}